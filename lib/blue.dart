import 'package:flutter/material.dart';

class Blue extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Blue'),
      ),
      body: Container(height: double.infinity, width: double.infinity, color: Colors.blue,),
    );
  }
}
