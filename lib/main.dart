import 'package:flutter/material.dart';

import 'blue.dart' deferred as blue;
import 'green.dart' deferred as green;
import 'red.dart' deferred as red;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RGB Deferred Component Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  final Future<void> _redFuture = red.loadLibrary();
  final Future<void> _greenFuture = green.loadLibrary();
  final Future<void> _blueFuture = blue.loadLibrary();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('RGB Deferred Component Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<void>(
              future: _redFuture,
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }
                  return TextButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => red.Red()));
                    },
                    child: Text(
                      'Red',
                      style: Theme.of(context)
                          .textTheme
                          .headline4!
                          .apply(color: Colors.red),
                    ),
                  );
                }
                return const CircularProgressIndicator();
              },
            ),
            FutureBuilder<void>(
              future: _greenFuture,
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }
                  return TextButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => green.Green()));
                    },
                    child: Text(
                      'Green',
                      style: Theme.of(context)
                          .textTheme
                          .headline4!
                          .apply(color: Colors.red),
                    ),
                  );
                }
                return const CircularProgressIndicator();
              },
            ),
            FutureBuilder<void>(
              future: _blueFuture,
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }
                  return TextButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => blue.Blue()));
                    },
                    child: Text(
                      'Blue',
                      style: Theme.of(context)
                          .textTheme
                          .headline4!
                          .apply(color: Colors.red),
                    ),
                  );
                }
                return const CircularProgressIndicator();
              },
            ),
          ],
        ),
      ),
    );
  }
}
