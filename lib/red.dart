import 'package:flutter/material.dart';

class Red extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Red'),
      ),
      body: Container(height: double.infinity, width: double.infinity, color: Colors.red,),
    );
  }
}
