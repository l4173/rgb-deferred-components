import 'package:flutter/material.dart';

class Green extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Green'),
      ),
      body: Container(height: double.infinity, width: double.infinity, color: Colors.green,),
    );
  }
}
